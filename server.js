//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});
var requestjson= require('request-json');
var urlMovimientos = "https://api.mlab.com/api/1/databases/cmontesdeoca/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlMovimientos);

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  res.sendFile(path.join(__dirname,'index.html'));
});

app.put("/", function(req, res){
  res.send("Hemos recibido su peticion put cambiado");
})

app.delete("/", function(req, res){
  res.send("Hemos recibido su peticion delete");
})

app.post("/", function(req, res){
  res.send("Hemos recibido su peticion post");
})

app.get("/clientes/:idcliente", function(req, res){
  var mijson = "{'idcliente':12345}";
  //res.send("Aqui tienes al clientes numero:" + req.params.idcliente);
  res.send(mijson);
});

app.get("/v1/movimientos", function(req, res){
  res.sendFile(path.join(__dirname,'movimientov1.json'));
});

var movimientosJSON = require('./movimientov2.json');

app.get('/v2/movimientos',function(req, res){
  res.json(movimientosJSON);
})


app.get('/v2/movimientos/:id',function(req, res){
  console.log(req.params.id);
  res.json(movimientosJSON[req.params.id-1]);
})

app.get('/v2/movimientosq', function(req, res){
  console.log(req.query);
  res.send("recibido");
})

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimientos dado de alta");
})


app.get('/movimientos', function(req, res){
  clienteMlab.get('', function(err, resM, body){
    if(err){
      console.log(err);
    } else {
      res.send(body);
    }
  });
})


app.post('/movimientos', function(req, res){
  clienteMlab.post('',req.body,function(err,resp,body){
    if(err){
      console.log(err);
    } else {
      res.send(body);
    }
  });
})
